# LCD Image Converter

## Description

Simple program to convert a bitmat image (.bmp) to a C-Header file, to store image primitives in Flash memory for STM32 LCD applications.


## Build the code

```bash
$ gcc -o lcd-image-converter main.c
```

## How to use it?

Place your desired image named as `bitmap.bmp` in the same folder as the generated executable file, a `bitmap.h` file will be generated after running `$ ./lcd-image-converter`. 
